<?php
// function calculate amount of month by recursive
function calMonthRecursive($money, $rate){
    if($money>2){
        return log($money,1+$rate);
    }
    return calMonthRecursive($money*(1+$rate),$rate);
}

// function calculate amount of month normally
function calMonthNotRecursive($money, $rate){
    $funds=$money;
    $month=0;
   while($funds<$money*2){
    $funds+=$funds*($rate/100);
    $month++;
   }
   return $month;
}

echo calMonthRecursive(1,0.1);
echo calMonthNotRecursive(1000,10);
?>