<?php
//class Product
class Product{
    private $name;
    private $price;
    private $quality;
    private $categoryId;

    public function __construct($name, $price, $quality, $categoryId)
    {
        $this->name=$name;
        $this->quality=$quality;
        $this->price=$price;
        $this->categoryId=$categoryId;
    }

    public function processInfomation()
    {
        return $this->name.'|'.$this->price.'|'.$this->quality.'|'.$this->categoryId;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getQuality(){
        return $this->quality;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getCategoryId(){
        return $this->categoryId;
    }
}

//class Category
class Category{
    private $id;
    private $name;

    public function __construct($id,$name )
    {
        $this->name=$name;
        $this->id=$id;
    }

    public function processInfomation()
    {
        return $this->name;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCategoryName()
    {
        return $this->name;
    }

    public function setCategoryName($value){
        $this->name=$value;
    }
}

//INSERT DATA INTO ARRAY ZONE

//insert product
$productList=[
    new Product('CPU',750,10,1),
    new Product('RAM',50,2,2),
    new Product('HDD',70,1,2),
    new Product('Main',400,3,1),
    new Product('Keyboard',30,8,4),
    new Product('Mouse',25,50,4),
    new Product('VGA',60,35,3),
    new Product('Monitor',120,28,2),
    new Product('Case',120,28,5),

];

//insert category
$categoryList=[
   new Category(1,'Computer'),
   new Category(2,'Memory'),
   new Category(3,'Card'),
   new Category(4,'Ascesory'),
];

//SOLVED MAIN

//FIND PRODUCT BY PRICE
//name product need to find
$name_product='CPU';
echo 'The results of finding product by price:';
findPrice($productList,750);
echo '<br/>';
echo '======================================<br/>';
//SORT BY PRICE
echo 'The results of sort product list by price (Bubble Sort):<br/>';
$productList_sorted=sortByPrice($productList);
foreach($productList_sorted as $item){
echo $item->processInfomation();
echo "<br>";


}
echo '======================================<br/>';
echo 'Product has max price is: ';
//Get Max By Price
echo maxByPrice($productList)->processInfomation().'<br/>';
echo 'Product has max price is: ';
//Get Min By Price
echo minByPrice($productList)->processInfomation().'<br/>';
echo '======================================<br/>';
echo 'The results of sort product list by Name Length (Insertion Sort):<br/>';
$productList_sort_name=sortByName($productList);
foreach($productList_sort_name as $item){
echo $item->processInfomation();
echo "<br>";
}
echo '======================================<br/>';
echo 'The results of sort product list by Name Category (Insertion Sort):<br/>';
$productListSortedByCategoryName=sortByCategoryName($productList,$categoryList);
foreach($productListSortedByCategoryName as $item){
echo $item->processInfomation();
echo "<br>";
}
echo '======================================<br/>';
echo 'The results of map product by category:<br/>';
mapProductByCategory($productList,$categoryList);
//FUNCTION FINDING ZONE

//function find product by name
 function findProduct($listProduct, $nameProduct){
    foreach($listProduct as $item){
        if($item->getName()==$nameProduct){
            return $item->processInfomation();
        }
    }  
}

//function find product by price
 function findPrice($listProduct, $price){
    foreach($listProduct as $item){
        if($item->getPrice()==$price){
            return $item->processInfomation();
        }
    }  
}

//function find categoryById
function findCategoryById($id,$categoryList){
    foreach($categoryList as $item){
        if($item->getId()==$id){
            return $item;
        }    
    } 
}


//FUNCTION SORT ZONE

//function sortByPrice by bubble sort
function sortByPrice($listProduct){
    for ($i=1; $i < Count($listProduct); $i++) { 
        $sorted=false;
        for ($j = 0; $j < Count($listProduct)-1; $j++)
        {
            if ($listProduct[$j]->getPrice() > $listProduct[$j+1]->getPrice())
            {
                $temp=$listProduct[$j];
                $listProduct[$j]=$listProduct[$j+1];
                $listProduct[$j+1]=$temp;
            }
            $sorted=true;
        }
        if($sorted==false)
        break;
    }
    return $listProduct;
}

//function sortByName by insertion sort
function sortByName($listProduct){
    for ($i = 1; $i < Count($listProduct); $i++)
    {
        $key = $listProduct[$i];
        $j = $i - 1;
        while ($j >= 0 && strlen($listProduct[$j]->getName()) > strlen($key->getName()))
        {
            $listProduct[$j + 1] = $listProduct[$j];
            $j = $j - 1;
        }
        $listProduct[$j + 1] = $key;
    }
    return $listProduct;
}


//function sortByCategoryName
function sortByCategoryName($listProduct,$listCategory){
    $temp=null;
    for ($i = 1; $i < Count($listProduct); $i++)
    {
        $key = $listProduct[$i];
        $j = $i - 1;   
        $cateNameAtKey=findCategoryById($key->getCategoryId(),$listCategory);
        $cateNameAtJ=findCategoryById($listProduct[$j]->getCategoryId(),$listCategory);
        if(empty($cateNameAtKey)){
            $temp='Z';
        }
        else $temp=$cateNameAtKey->getCategoryName();
        while ($j >= 0&&$cateNameAtJ->getCategoryName()>$temp)
        {
            $listProduct[$j + 1] = $listProduct[$j];
            $j = $j - 1;
        }
        $listProduct[$j + 1] = $key;
    }
    return $listProduct;
    
}

//FUNCTION GET MIN/MAX ZONE

//function get max price
function maxByPrice($listProduct){
    $listProduct1=sortByPrice($listProduct);
    return $listProduct1[Count($listProduct1)-1];
}

//function get min price
function minByPrice($listProduct){
    $listProduct1=sortByPrice($listProduct);
    return $listProduct1[0];
}

//FUNCTION MAP ZONE

//function mapProductByCategory
function mapProductByCategory($listProduct,$listCategory){
    foreach ($listProduct as $item) {
        $getCateName=0;
        $cateName=findCategoryById($item->getCategoryId(),$listCategory);
        if($cateName==null){
            $getCateName=empty($cateName);
        }
        else $getCateName=$cateName->getCategoryName();
        echo $item->processInfomation().'|'.$getCateName;
        echo '<br/>';
    }
}
    

?>