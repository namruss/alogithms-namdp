<?php
$menu = array(
    1 => array(
        'id' => 1,
        'title' => "Thể thao",
        'parent_id' => 0
    ),
    2 => array(
        'id' => 2,
        'title' => "Xã hội",
        'parent_id' => 0
    ),
    3 => array(
        'id' => 3,
        'title' => "Thể thao trong nước",
        'parent_id' => 1
    ),
    4 => array(
        'id' => 4,
        'title' => "Giao thông",
        'parent_id' => 2
    ),
    5 => array(
        'id' => 5,
        'title' => "Môi trường",
        'parent_id' => 2
    ),
    6 => array(
        'id' => 6,
        'title' => "Thể thao quốc tế",
        'parent_id' => 1
    ),
    7 => array(
        'id' => 7,
        'title' => "Môi trường đô thị",
        'parent_id' => 5
    ),
    8 => array(
        'id' => 8,
        'title' => "Giao thông ùn tắc",
        'parent_id' => 4
    ),
);

//function xu ly du lieu theo cap bac
function getRecord($menu,$parentId,$level=0){
        $dataList=[];
        foreach($menu as $item){
            if($item['parent_id']==$parentId){
                    $item['level']=$level;
                    $dataList[]=$item;
                    unset($menu[$item['parent_id']]);
                    $sons=getRecord($menu,$item['id'],$level+1);
                    $dataList=array_merge($dataList,$sons);
            }
        }
        return $dataList;
}


$list=getRecord($menu,0);
foreach($list as $item){
    echo str_repeat('--',$item['level']).''.$item['title'];
    echo "<br/>";
}

?>