<?php
//function to calculate salary by recursive
function calSalaryRecursive($salary, $n)
{
    if($n<1){
        return $salary;
    }
    return calSalaryRecursive($salary,$n-1)*1.1;
}

//function to calculate salary normal
function calSalaryNotRecursive($salary, $n)
{
    while ($n > 0) {
        $salary = $salary + ($salary * 10) / 100;
        $n = $n - 1;
    }
    return $salary;
}

print_r(calSalaryNotRecursive(5000, 10));
echo "<br/>";
print_r(calSalaryRecursive(5000, 10));
