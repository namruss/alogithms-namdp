<?php
//class Stack
class Stack
{
    function push($input, $member)
    {
        array_push($member, $input);
        return $member;
    }

    function get($member)
    {
        $lastValue = array_pop($member);
        return $lastValue;
    }
}

//class Queue
class Queue
{
    function push($input, $member)
    {
        array_unshift($member, $input);
        return $member;
    }

    function get($member)
    {
        $member = array_shift($member);
        return $member;
    }
}

//function to print list
function printList($list)
{
    foreach ($list as $item) {
        echo $item . '-';
    }
}   

// //Solve Stack
$array = [1, 2];
// $stack = new Stack();
// printList($stack->push('1', $array));
// echo "<br/>";
// echo $stack->get($array);

//Solve Queue
$queue = new Queue();
printList($queue->push('1', $array));
echo "<br/>"; 
echo $queue->get($array);
